自制基于mybatis简易数据库交互组件，用户引入jar包后，只要在dao层继承BaseDao类接口，就能实现类似于jpa规划似的数据库操作，无需在配置mapper.xml和写sql代码，数据通过参数传入即可。文章末尾提供jar包的下载地址，有兴趣的小伙伴可以下载试试。
 **
一、需求背景及需求目标** 
1.需求背景：mybatis提供了便捷的数据库cudr操作的方式，但是传统的mybatis的使用，需要根据数据库表的映射，形成一一对应的实体类，以及创建复杂对应实体的mapper.xml 的映射文件，sql语句编写也比较麻烦，还是需要在mapper中窜写完整的sql语句脚本，影响开发效率，所以个人在此背景下自制封装了该组件，力求尽量不写mapper.xml，不写sql语句，只通过像类方法一样调用cudr操作，数据通过传参即可完成，用户只要写dao层就行。

2.需求目标：通过该组件，实现与数据库的cudr操作，不在通过mapper.xml来进行配置，尽量不写sql语句，而是像普通方法一样，简单调用存储，或者更新等方法，传入相应的参数即可，组件自动帮我生成sql与数据库进行操作；同时抛弃传统的实体类与数据库表以及与mapper.xml文件必须一一对应的关系，更加自由的编写相关dao层代码。

 **二、组件功能和使用** 
组件主要功能：

提供像类方法一样的对mysql数据库cudr操作，无需编写mapper.xml配置及传统sql语句；库表名称、字段通过参数进行传入相应方法即可。
提供数据库与实体类自动映射，无需通过xml配置，且通过类似jpa规范的@column注释标签，可以实现通过标签注释来实现与库表字段进行映射，而不用要求实体属性名必须与库表字段名保持一致，或需要实体类要求在某个特定的扫描包下。
提供分页查询功能，传入相应的参数，即可查出分页的内容
提供log4j日志打印，每调用相关的cudr操作的方法，控制台都会打印出自动生成的相关sql语句日志，可用于日后debug等操作。
组件的使用：
前提：该组件依赖与spring框架及mybatis组件框架，所以在使用时请确保已经引入了相关依赖的jar包（spring项目和springboot项目都能直接使用），以及因为组件还使用了log4j日志打印功能，所以也需要依赖与log4j的jar包。

组件已经打包成jar包的形式，普通web项目可以直接把jar把放到相应的lib目录下就能使用，maven项目可以在任意目录下创建lib包，将jar包放入后，pom.xml配置里加入下面这段话引入jar包
autoCreateSql
autoCreateSql
1.0
system

${project.basedir}/src/main/lib/noSqlMybatis-1.0-SNAPSHOT.jar
引入组件jar包后就可以正式开始使用了.
在相应的dao层类中，只要继承BaseDao，就能使用相应的增删改查的方法了，无需mapper.xml配置。
示例如下：
在这里插入图片描述

对应库表数据的实体类代码示例如下：

在这里插入图片描述

实体类提供了@Column标签注释，可以通过该标签来实现与数据库字段的一一对应，如果属性没有该标签，则根据属性名称与数据库字段对应。（即@Column注释里的值要与库表字段名称一致；或者不用@Column，属性的名称要和库表字段名称一致）

 **三、BaseDao类提供的CUDR方法使用说明：** 
findByEx
功能：条件查询具体某个结果

参数及说明：
1）BaseDaoQuery query： 查询条件。如果为null，则不带查询条件查询（后文会有查询条件query方法的相关说明）；
2）List dataBaseName（必填）： 需要查询的数据库表名称。之所以用集合的方式是，组件提供了内联查询，支持多表关联查询，所以有涉及到多表时，可以以集合的方式传入多个库表名称
3）List result： 需要查询的结果字段集合。如果为null，则查询全部，相当于select *；需要具体查询某个字段时，可以集合方式传入
4）Class target（必填）： 查询出的结果实体类。用以映射查询结果的类

返回值：
Class 与数据库表字段映射的实体类

示例：

在这里插入图片描述

findListByEx
功能：条件查询结果集合

参数及说明:
1）BaseDaoQuery query： 查询条件（同上说明）
2）List dataBaseName（必填）： 要查询的数据库表名称（同上说明）
3）List result： 需要查询的结果字段集合（同上说明）
4）Class target（必填）： 查询出的结果实体类，用以映射查询结果的类

返回值：
List 返回传入的结果类集合

示例：
在这里插入图片描述

findPageByEx
功能：分页条件查询数据库表结果

参数及说明：
1）BaseDaoQuery query：查询条件（同上说明）
2）List dataBaseName（必填）：要查询的数据库表名称集合（同上说明）
3）List result：需要查询的结果字段名称集合（同上说明）
4）Page page（必填）： 分页信息；组件自带的类，提供存储分页的信息，及分页的数据（具体可查看源码）；作为参数时传入时续提供每页展示数量pageSize，和起始页pageNo；默认初始化page时每页数量为15，起始页为1
5）Class target（必填）：查询出的数据结果实体类（同上说明）

返回值：
Page 将返回一个page类，page属性包括分页的信息，起始页，每页条数，是否有下一页，查询到的结果数据，结果总条数。

示例：

在这里插入图片描述

findOne
功能：条件查询某个具体值

参数及说明：
1） BaseDaoQuery query：查询条件（同上说明）
2） List dataBaseName（必填）：要查询的数据库表名称集合（同上说明）
3） String result（必填）： 要查询的数据库具体字段名称

返回值：
String 返回具体的某个字段值，如统计用户数量啊，查询参数的数据库字段名称填“count(*)”
查询结果就为，用户数量50；

示例：
在这里插入图片描述

findListOne
功能：条件查询某个具体值集合

参数及说明：
1） BaseDaoQuery query：查询条件（同上说明）
2） List dataBaseName（必填）：要查询的数据库表名称集合（同上说明）
3） String result（必填）： 要查询的数据库具体字段名称
4）
返回值：
List 返回查询的字段值的集合，如查询年龄都在18岁所有用户的名字

示例：
在这里插入图片描述

save
功能：保存（插入）实体类信息到指定库表

参数及说明：
1） String dataBaseName（必填）： 要插入的指定库表名称
2） Object target（必填）： 需要保存的实体类

返回值：
Int 返回插入成功的执行条数

示例：
在这里插入图片描述

updateByEx
功能：指定条件更新库表字段

参数及说明：
1） String dataBaseName（必填）： 要指定修改的库表名称
2） BaseDaoQuery query： 查询条件（同上说明）；如果为null，则更新库表所有字段
3） Object target（必填）： 需要更新的实体类，如果实体类中的某个属性值为null，则该值不参与更新

返回值：
Int 更新成功操作数据库的条数

示例：
在这里插入图片描述

deleteByEx
功能：指定条件删除库表信息

参数及说明：

String dataBaseName（必填）： 要指定删除数据的库表名称
BaseDaoQuery query： 条件语句（同上说明）；如果该参数为null，可能删除该库表下所以数据
返回值：
int 删除成功的数据操作条数

示例：
在这里插入图片描述

 **四、关与BaseDaoQuery 查询条件类的使用** 
BaseDaoQuery提供了用于条件查询的一些方法，使原先where的写法，参照一些类似jpa规范，传参即可完成where条件语句的编写。原理就是将参数转变为 sql让mybatis识别，具体可以下载源码下来查看，就是一些简单的封装，这边先介绍用法吧

实例化BaseDaoQuery，传入要具体实现功能的条查询类Criteria，调用Criteria的方法传参来实现查询的条件，因为方法返回的值都还是Criteria，所以可以接在后面继续调用，直到指定条件都传入为止。BaseDaoQuery类也提供了or方法，可以传入不止一个Criteria查询类，用以应对多种不同的查询情况。(BaseDaoQuery方法中的ordeyBy和distinct方法2期版本还在开发，尚不能使用)
示例：
BaseDaoQuery query =BaseDaoQuery.Query(new Criteria()
.andConditionEqualTo(“id”, id)
.andConditionEqualTo(“available”,1)
);
Criteria下提供了一些类似jpa规范的方法，用户在使用使用时选择相应的方法，传入参数即可，方法如下：
andConditionIsNull(String condition)
说明：数据库表某个字段为空 相当于 where a is null;
参数：
参数1：数据库表为空的字段名

andConditionNotNull(String condition)
说明：数据库表某个字段不为空 相当于 where a is not null;
参数：
参数1：数据库表不为空的字段名

andConditionEqualTo(String condition, Object value)
说明：数据库表某个字段的值等于传入的值，相当于where a=b;
参数：
参数1：数据库表做比较的字段名
参数2：拿来做比较的值

andConditionNotEqualTo(String condition, Object value)
说明：数据库表某个字段的值不等于传入的值，相当于where a <> b;
参数：
参数1：数据库表做比较的字段名
参数2：拿来做比较的值

andConditionGT(String condition, Object value)
说明：数据库表某个字段的值大于传入的值，相当于where a > b;
参数：
参数1：数据库表做比较的字段名
参数2：拿来做比较的值

andConditionGTE(String condition, Object value)
说明：数据库表某个字段的值大于等于传入的值，相当于where a >= b;
参数：
参数1：数据库表做比较的字段名
参数2：拿来做比较的值

andConditionLT(String condition, Object value)
说明：数据库表某个字段的值小于传入的值，相当于where a < b;
参数：
参数1：数据库表做比较的字段名
参数2：拿来做比较的值

andConditionLTE(String condition, Object value)
说明：数据库表某个字段的值小于等于传入的值，相当于where a <= b;
参数：
参数1：数据库表做比较的字段名
参数2：拿来做比较的值
 
andConditionIn(String condition, List values)
说明：数据库表某个字段的属于传入的值的集合中的某一个，相当于where a in ( b,c,……)
参数：
参数1：数据库表字段名
参数2：传入用于in 的值的集合

andConditionNotIn(String condition, List values)
说明：数据库表某个字段的不属于传入的值的集合中的某一个，相当于where a not in ( b,c,……)
参数：
参数1：数据库表字段名
参数2：传入用于not in 的值的集合

andConditionBetween(String condition, Object value1, Object value2)
说明：数据库表某个字段在某两个值之间，相当于where a between b and c；
参数：
参数1：数据库表字段名
参数2：传入范围的第一个值
参数3：传入范围的第二个值

andConditionNotBetween(String condition, Object value1, Object value2)
说明：数据库表某个字段不在某两个值之间，相当于where a not between b and c；
参数：
参数1：数据库表字段名
参数2：传入范围的第一个值
参数3：传入范围的第二个值

andConditionRightLike(String condition, Object value)
说明：数据库表某个字段右模糊查询，相当于where a like %c；
参数：
参数1：数据库表字段名
参数2：传入右模糊查询的值
andConditionLeftLike(String condition, Object value)
说明：数据库表某个字段左模糊查询，相当于where a like c%；
参数：
参数1：数据库表字段名
参数2：传入左模糊查询的值

andConditionAllLike(String condition, Object value)
说明：数据库表某个字段全模糊查询，相当于where a like %c%；
参数：
参数1：数据库表字段名
参数2：传入全模糊查询的值

andConditionNotLike(String condition, Object value)
说明：数据库表某个字段不匹配，相当于where a not like %c%；
参数：
参数1：数据库表字段名
参数2：传入不匹配查询的值

 **五、结语** 
说是自制组件，其实就是在原mybatis接口上封装了一些方法，让原先接口读取的mapper.xml变成读取类提供的sql语句，而类的sql通过用户调用方法传参来自动生成；BaseDaoQuery类参考了Mybatis Generator自动生成映射实体类和接口方法的插件，其中有一个Example类提供了各个实体类的条件查询方法，我对其经写了一些改造，将固定某个实体类的查询方法改成通过传参的方式传入，这样只要一个BaseDaoQuery就能够通用各个实体了。而@Column标签注释是后面突然奇想加的，既然都不用映射数据库实体了，那干脆字段名也不用一致吧，就从jpa的注释那搬了一个这个标签过来，目前也只有这一个，后续是不是加入其他的再说吧。

我知道，虽然市面上已经有很多比我封装的这个组件更好的与数据库交互的替代方式，但是如果是个人编写，而不是公司项目的话，我觉得就没有必要搬那么重的东西，自己编写的也挺开心，如果有人愿意用，或者帮我提出什么bug，我就更开心了，希望得到到你的留言回复。个人微信公众号：九代的梦想吐槽。微博：z忘却的九九

