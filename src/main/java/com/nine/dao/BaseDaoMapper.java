package com.nine.dao;


import com.nine.dao.dto.BaseDaoQuery;
import com.nine.dao.dto.BaseKeyValueDto;
import com.nine.dao.dto.Context;
import com.nine.dao.dto.Page;
import com.nine.dao.sql.BaseSQLToString;


import org.apache.ibatis.annotations.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Map;

/**
 * 基础dao映射接口（自制组件）
 *
 * @author zjb
 */
@Repository
@Mapper
public interface BaseDaoMapper<T> {



    /**
     * 查询某个特定值接口，返回数据库字段不能为空，且应该只能为一个
     *
     * @param query        查询参数
     * @param dataBaseName 库表名称
     * @param result       返回哪些字段
     * @param page         分页 ,查某一项值时分页应该不需要，传null就行
     * @return
     */
    @SelectProvider(type = BaseSQLToString.class, method = "selectExample")
    List<String> findOneByExample(BaseDaoQuery query, List<String> dataBaseName, List<String> result, Page page);

    /**
     * 插入库表字段，返回插入成功数量
     *
     * @param dataBaseName 库表名称
     * @param entity    传入实体
     */
    @InsertProvider(type = BaseSQLToString.class, method = "insertExample")
    int insertByValues(String dataBaseName, Object entity);

    /**
     * 执行更新，返回更新影响条数
     *
     * @param dataBaseName 库表名
     * @param query        条件参数
     * @param valueDtos    需要更新的库表列名及具体值的键值对
     * @return
     */
    @UpdateProvider(type = BaseSQLToString.class, method = "updateExample")
    int updateByValues(String dataBaseName, BaseDaoQuery query, List<BaseKeyValueDto> valueDtos);

    /**
     * 执行删除，返回影响条数
     *
     * @param dataBaseName 库表名称
     * @param query        条件参数
     * @return
     */
    @DeleteProvider(type = BaseSQLToString.class, method = "deleteExample")
    int deleteByBaseDaoExample(String dataBaseName, BaseDaoQuery query);

    /**
     * 返回查询结果集合，以map键值对形式返回，后续可以方便自行转换为需要的具体类
     *
     * @param query
     * @param dataBaseName
     * @param result
     * @param page
     * @return
     */
    @SelectProvider(type = BaseSQLToString.class, method = "selectExample")
    List<Map<String, Object>> findListByExample(BaseDaoQuery query, List<String> dataBaseName, List<String> result, Page page);

}
