package com.nine.dao.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


@Target({METHOD, FIELD})
@Retention(RUNTIME)
public @interface Column {
    /**
     * 用来映射数据库文件名称
     * 当属性字段不想和数据库字段保持一致，可以用该注释
     */
    String name() default "";
}
