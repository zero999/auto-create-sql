package com.nine.dao.dto;



import java.io.Serializable;
import java.util.ArrayList;



public class Page<T> implements Serializable {
    /**
     * 每页数量
     */
    public int pageSize = 15;
    /**
     * 起始页
     */
    public int pageNo = 1;
    /**
     * 数据
     */
    public T data;
    /**
     * 总数量
     */
    public long totalSize;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }

    public Page() {
        this(1, 15, 0L, null);
    }

    public Page(int PageNo, int PageSize, long totalSize, T data) {
        this.pageSize = PageSize;
        this.pageNo = PageNo;
        this.totalSize = totalSize;
        this.data= data;
    }

    /**
     * 取总页数
     */
    public long getTotalPageSize() {
        if (this.totalSize == 0) {
            return 0;
        } else {
            long TotalPageSize = this.totalSize / pageSize;
            return this.totalSize % pageSize == 0 ? TotalPageSize : TotalPageSize + 1;
        }
    }

    /**
     * 是否有下一页
     */
    public boolean hasNextPage() {
        return (this.pageNo < this.getTotalPageSize());
    }

    /**
     * 是否有上一页
     */
    public boolean hasPreviousPage() {
        return (this.pageNo > 1);
    }
}
