package com.nine.dao.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * 改造自动生成mybatis工具生成的查询类 (自制组件)
 * 调用方法时，需要conditon字段值和数据库字段一致
 *
 * @author zjb
 */
public class BaseDaoQuery {

    //排序和去从没有做完，这个功能没有做出来啊,本身应该还有排序的
    protected String orderByClause;
    //默认升序
    protected String orderByType="ASC";

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BaseDaoQuery() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * 不传排序方式，默认升序
     * @param orderByClause
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * 设置排序方式，升序还是降序
     * @param orderByClause
     * @param orderType
     */
    public void setOrderByClause(String orderByClause,String orderType) {
        this.orderByClause = orderByClause;
        this.orderByType=orderType;
    }
    public String getOrderByClause() {
        return orderByClause;
    }

    public String getOrderByType() {
        return orderByType;
    }

    public void setOrderByType(String orderByType) {
        this.orderByType = orderByType;
    }

    public void setOredCriteria(List<Criteria> oredCriteria) {
        this.oredCriteria = oredCriteria;
    }



    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    public static BaseDaoQuery Query(Criteria criteria) {
        return new BaseDaoQuery(criteria);
    }

    public BaseDaoQuery(Criteria criteria) {
        oredCriteria = new ArrayList<Criteria>();
        oredCriteria.add(criteria);
    }



    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }
}
