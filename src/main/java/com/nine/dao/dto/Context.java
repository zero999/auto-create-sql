package com.nine.dao.dto;


import org.springframework.stereotype.Component;

/**
 * 功能描述：ThreadLocal 用于线程值传递
 *
 * @Author： zhangjb
 */
@Component
public class Context<T> {

    ThreadLocal<T> local = new ThreadLocal();


    public T getInfo() {
        return local.get();
    }

    public void setInfo(T tar) {
        local.set(tar);
    }

    public void remove() {
        local.remove();
    }
}
