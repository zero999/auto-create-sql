package com.nine.dao.dto;

/**
 * 改造自动生成mybatis工具生成的查询类 (自制组件)
 * 调用方法时，需要conditon字段值和数据库字段一致
 *
 * @author zjb
 */
public class Criteria extends GeneratedCriteria {
    public Criteria() {
        super();
    }
}
