package com.nine.dao.dto;




/**
 * 基本的类似键值对dto(自制组件)
 * 只有title,和value;
 *
 * @author zjb
 */

public class BaseKeyValueDto<k,v> {


    private k title;


    private v value;

    public k getTitle() {
        return title;
    }

    public void setTitle(k title) {
        if (title == null || title.equals("")) {
            throw new RuntimeException("title不能为空");
        }
        this.title = title;
    }

    public v getValue() {
        return value;
    }

    public void setValue(v value) {
        this.value = value;
    }
}
