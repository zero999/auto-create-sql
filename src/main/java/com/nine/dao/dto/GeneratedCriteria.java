package com.nine.dao.dto;



import java.util.ArrayList;
import java.util.List;

/**
 * 改造自动生成mybatis工具生成的查询类 (自制组件)
 * 调用方法时，需要conditon字段值和数据库字段一致
 *
 * @author zjb
 */
public abstract class GeneratedCriteria {
    protected List<Criterion> criteria;

    public GeneratedCriteria() {
        super();
        criteria = new ArrayList<Criterion>();
    }

    public boolean isValid() {
        return criteria.size() > 0;
    }

    public List<Criterion> getAllCriteria() {
        return criteria;
    }

    public List<Criterion> getCriteria() {
        return criteria;
    }

    protected void addCriterion(String condition) {
        if (condition == null) {
            throw new RuntimeException("Value for condition cannot be null");
        }
        criteria.add(new Criterion(condition));
    }

    protected void addCriterion(String condition, Object value) {
        if (value == null) {
            throw new RuntimeException("Value for " + condition + " cannot be null");
        }
        criteria.add(new Criterion(condition, value));
    }

    protected void addCriterion(String condition, Object value1, Object value2) {
        if (value1 == null || value2 == null) {
            throw new RuntimeException("Between values for " + condition + " cannot be null");
        }
        criteria.add(new Criterion(condition, value1, value2));
    }


    public Criteria andConditionIsNull(String condition) {
        addCriterion(condition + " is null");
        return (Criteria) this;
    }

    public Criteria andConditionNotNull(String condition) {
        addCriterion(condition + " is not null");
        return (Criteria) this;
    }

    public Criteria andConditionEqualTo(String condition, Object value) {
        checkList(value);
        addCriterion(condition + " =", value);
        return (Criteria) this;
    }

    public Criteria andConditionNotEqualTo(String condition, Object value) {
        checkList(value);
        addCriterion(condition + " <>", value);
        return (Criteria) this;
    }

    public Criteria andConditionGT(String condition, Object value) {
        checkList(value);
        addCriterion(condition + " >", value);
        return (Criteria) this;
    }

    public Criteria andConditionGTE(String condition, Object value) {
        checkList(value);
        addCriterion(condition + " >=", value);

        return (Criteria) this;
    }

    public Criteria andConditionLT(String condition, Object value) {
        checkList(value);
        addCriterion(condition + " <", value);
        return (Criteria) this;
    }

    public Criteria andConditionLTE(String condition, Object value) {
        checkList(value);
        addCriterion(condition + " <=", value);
        return (Criteria) this;
    }

    public Criteria andConditionIn(String condition, List<String> values) {
        addCriterion(condition + " in", values);
        return (Criteria) this;
    }

    public Criteria andConditionNotIn(String condition, List<String> values) {
        addCriterion(condition + " not in", values);
        return (Criteria) this;
    }

    public Criteria andConditionBetween(String condition, Object value1, Object value2) {
        checkList(value1);
        checkList(value2);
        addCriterion(condition + " between", value1, value2);
        return (Criteria) this;
    }

    public Criteria andConditionNotBetween(String condition, Object value1, Object value2) {
        checkList(value1);
        checkList(value2);
        addCriterion(condition + " not between", value1, value2);
        return (Criteria) this;
    }


    public Criteria andConditionRightLike(String condition, Object value) {
        checkList(value);
        addCriterion(condition + " like", "%"+value);
        return (Criteria) this;
    }
    public Criteria andConditionLeftLike(String condition, Object value) {
        checkList(value);
        addCriterion(condition + " like", value+"%");
        return (Criteria) this;
    }

    public Criteria andConditionAllLike(String condition, Object value) {
        checkList(value);
        addCriterion(condition + " like", "%"+value+"%");
        return (Criteria) this;
    }

    /**
     * 不包含，只提供泛like
     * @param condition
     * @param value
     * @return
     */
    public Criteria andConditionNotLike(String condition, Object value) {
        checkList(value);
        addCriterion(condition + " not like", "%"+value+"%");
        return (Criteria) this;
    }

    /**
     * 校验传入值是否为集合
     * 如果非 in 查询，不允许传入集合
     * @param value
     * @return
     */
    private boolean checkList(Object value){
        if (value == null) {
            throw new RuntimeException("Value cannot be null");
        }
        if (value instanceof List<?>) {
            throw new RuntimeException("该类查询不允许传入集合参数");
        } else {
            return  true;

        }
    }
}
